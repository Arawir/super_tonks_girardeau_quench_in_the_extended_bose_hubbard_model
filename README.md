# Super_Tonks_Girardeau_Quench_in_the_Extended_Bose_Hubbard_Model

The archive includes all the data and scripts used to create the figures referenced in the article. For conciseness, the repository is organized into directories that align with the article's sections. Within each directory, you'll discover raw data files in text format, scripts for plot generation in .ipynb (Jupyter Notebook) format, and the finalized versions of the figures in PDF format.
